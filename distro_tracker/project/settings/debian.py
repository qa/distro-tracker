"""Debian.org specific settings"""

import os.path

from . import defaults
from .db_postgresql import DATABASES  # noqa
from distro_tracker.signon import providers

__all__ = [
    'ALLOWED_HOSTS',
    'DATABASES',
    'DISTRO_TRACKER_CVE_URL',
    'DISTRO_TRACKER_DEBCI_URL',
    'DISTRO_TRACKER_DEBCI_REPOSITORIES',
    'DISTRO_TRACKER_DEBIAN_PIUPARTS_SUITES',
    'DISTRO_TRACKER_DEVEL_REPOSITORIES',
    'DISTRO_TRACKER_FQDN',
    'DISTRO_TRACKER_REMOVALS_URL',
    'DISTRO_TRACKER_VENDOR_RULES',
    'DISTRO_TRACKER_VCS_TABLE_FIELD_TEMPLATE',
    'DJANGO_EMAIL_ACCOUNTS_PRE_LOGIN_HOOK',
    'INSTALLED_APPS',
    'enable_salsa_signon',
]

INSTALLED_APPS = defaults.INSTALLED_APPS.copy()
INSTALLED_APPS.extend([
    # Many debian.org customizations
    'distro_tracker.vendor.debian',
    # Generate warnings for outdated values of the Standards-Version field
    'distro_tracker.stdver_warnings',
    # Extract common files from the source package
    'distro_tracker.extract_source_files',
    # Debci status
    'distro_tracker.debci_status',
])

# Official service name
DISTRO_TRACKER_FQDN = "tracker.debian.org"
ALLOWED_HOSTS = [
    DISTRO_TRACKER_FQDN,
    "2qlvvvnhqyda2ahd.onion",
    "7fgb4sq435vg7slw3u7m2ayze3imybpe7qm3htdklreoag3l6n2mtkyd.onion",
]

# Custom data path (used only if it exists, so that we can reuse
# those settings in a development environment too).
if os.path.isdir('/srv/tracker.debian.org/data'):
    DISTRO_TRACKER_DATA_PATH = '/srv/tracker.debian.org/data'
    __all__.append('DISTRO_TRACKER_DATA_PATH')

if os.path.isfile('/etc/ssl/ca-global/ca-certificates.crt'):
    DISTRO_TRACKER_CA_BUNDLE = '/etc/ssl/ca-global/ca-certificates.crt'
    __all__.append('DISTRO_TRACKER_CA_BUNDLE')

#: A module implementing vendor-specific hooks for use by Distro Tracker.
#: For more information see :py:mod:`distro_tracker.vendor`.
DISTRO_TRACKER_VENDOR_RULES = 'distro_tracker.vendor.debian.rules'

#: A custom template which the vcs table field should use
DISTRO_TRACKER_VCS_TABLE_FIELD_TEMPLATE = 'debian/package-table-fields/vcs.html'

#: A list of suite names which should be used when updating piuparts stats
DISTRO_TRACKER_DEBIAN_PIUPARTS_SUITES = (
    'sid',
)

#: The page documenting package removals
DISTRO_TRACKER_REMOVALS_URL = "https://ftp-master.debian.org/removals.txt"

#: A list of the repositories where new versions are uploaded
DISTRO_TRACKER_DEVEL_REPOSITORIES = ['unstable', 'experimental']

#: URL for CVE tracker
DISTRO_TRACKER_CVE_URL = 'https://security-tracker.debian.org/tracker/'

#: repositories to check for debci status
DISTRO_TRACKER_DEBCI_REPOSITORIES = ['unstable', 'testing', 'stable']

#: URL for debci
DISTRO_TRACKER_DEBCI_URL = 'https://ci.debian.net'

#: disallow local auth for @debian.org email addresses
DJANGO_EMAIL_ACCOUNTS_PRE_LOGIN_HOOK = \
    'distro_tracker.vendor.debian.rules.pre_login'


def enable_salsa_signon(client_id, client_Secret):
    defaults.SIGNON_PROVIDERS.append(
        providers.GitlabProvider(
            name='salsa',
            label='Salsa',
            icon='signon/gitlabian.svg',
            client_id=client_id,
            client_secret=client_Secret,
            url='https://salsa.debian.org',
            scope=['openid', 'email'],
            restrict=['email-verified'],
        )
    )


try:
    with open('/var/lib/distro-tracker/salsa-client-id', 'r') as f:
        SALSA_CLIENT_ID = f.read().strip()
    with open('/var/lib/distro-tracker/salsa-client-secret', 'r') as f:
        SALSA_CLIENT_SECRET = f.read().strip()

    enable_salsa_signon(SALSA_CLIENT_ID, SALSA_CLIENT_SECRET)
except IOError:
    pass
