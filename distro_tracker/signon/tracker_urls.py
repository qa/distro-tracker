# Copyright 2024 The Debusine Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Distro Tracker. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution and at https://deb.li/DTLicense. No part of Distro Tracker,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.

"""debusine signon URLs Configuration."""
from django.urls import path

from distro_tracker.signon.views import (
    BindIdentityView,
    OIDCAuthenticationCallbackView,
)

urlpatterns = [
    path(
        "oidc_callback/<name>/",
        OIDCAuthenticationCallbackView.as_view(),
        name="oidc_callback",
    ),
    path(
        "bind_identity/<name>/",
        BindIdentityView.as_view(),
        name="bind_identity",
    ),
]
