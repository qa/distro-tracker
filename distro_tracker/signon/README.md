The code in this app has been copied from
[Debusine](https://salsa.debian.org/freexian-team/debusine/-/tree/devel/debusine/server/signon)
(revision 27c5690a1477a4a70e722d87e71ed6e36c41ddf3).
It has been adapted to work with Distro Tracker's user model and the redirect
targets have been modified to the respective Distro Tracker URLs.

The code should be kept in sync with Debusine and any improvements to it beyond
integration should be done there first.

Debusine's documentation on how to use SSO with GitLab mostly applies here as
well:
https://freexian-team.pages.debian.net/debusine/howtos/enable-logins-with-gitlab.html
