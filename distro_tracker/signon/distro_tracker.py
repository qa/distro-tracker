# Copyright 2024 The Debusine Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Distro Tracker. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution and at https://deb.li/DTLicense. No part of Distro Tracker,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.
"""Distro Tracker extensions to Signon."""

import logging

from django_email_accounts.models import User, UserEmail
from django.core.exceptions import ValidationError
from django.contrib.auth.hashers import make_password

from distro_tracker.signon.models import Identity
from distro_tracker.signon.signon import Signon
from distro_tracker.signon.utils import split_full_name

log = logging.getLogger(__name__)


class DistroTrackerSignon(Signon):
    """
    Distro Tracker specific extension to Signon.

    Activate it in django settings with::

        SIGNON_CLASS = "distro_tracker.signon.distro_tracker.DistroTrackerSignon"  # noqa: E501
    """

    def _lookup_user_from_identity(self, identity: Identity) -> User | None:
        """Lookup an existing user from claims in an Identity."""
        try:
            user_email = UserEmail.objects.get(
                email__iexact=identity.claims["email"]
            )
            return user_email.user
        except UserEmail.DoesNotExist:
            return None

    def create_user_from_identity(self, identity: Identity) -> User | None:
        email = identity.claims["email"]

        user_email, _ = UserEmail.objects.get_or_create(
            email__iexact=email, defaults={'email': email}
        )
        if not user_email.user:
            first_name, last_name = split_full_name(identity.claims["name"])

            # Django does not run validators on create_user, so garbage in the
            # claims can either create garbage users, or cause database
            # transaction errors that will invalidate the current transaction.
            #
            # See: https://stackoverflow.com/questions/67442439/why-django-does-not-validate-email-in-customuser-model  # noqa: E501

            # Instead of calling create_user, I instead have to replicate what
            # it does here and call validation explicitly before save.

            # This is the equivalent of the following, with validation:
            # user = User.objects.create_user(
            #     main_email=email,
            #     first_name=first_name,
            #     last_name=last_name,
            # )
            user = User(
                main_email=email,
                first_name=first_name,
                last_name=last_name,
                is_active=True,
            )
            user.password = make_password(None)
            try:
                user.clean_fields()
            except ValidationError as e:
                log.warning(
                    "%s: cannot create a local user",
                    identity,
                    exc_info=e,
                )
                return None
            user.save()
            user_email.user = user
            user_email.save()
        else:
            user = user_email.user

        return user
