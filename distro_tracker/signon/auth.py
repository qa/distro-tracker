# Copyright 2020-2023 Enrico Zini <enrico@debian.org>
# Copyright 2023 The Debusine Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Distro Tracker. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution and at https://deb.li/DTLicense. No part of Distro Tracker,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.

"""Authentication backend to mark signon-managed authentication."""

from django.contrib.auth.backends import ModelBackend


class SignonAuthBackend(ModelBackend):
    """
    Auth backend for external authentication.

    There is no specific functionality, and it is currently used to mark users
    authenticated via external signon providers.
    """

    pass
