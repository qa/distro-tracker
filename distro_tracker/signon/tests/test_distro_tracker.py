# Copyright 2024 The Debusine Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Distro Tracker. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution and at https://deb.li/DTLicense. No part of Distro Tracker,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.

"""Test the Debusine extensions to Signon."""

import contextlib
from collections.abc import Generator
from typing import Any, ClassVar
from unittest import mock

from django.test import RequestFactory, TestCase, override_settings

from distro_tracker.signon import providers
from distro_tracker.signon.distro_tracker import DistroTrackerSignon
from distro_tracker.signon.models import Identity
from django_email_accounts.models import User, UserEmail


@contextlib.contextmanager
def provider_options(**kwargs: Any) -> Generator[None, None, None]:
    """Set SIGNON_PROVIDERS with the given options."""
    with override_settings(
        SIGNON_PROVIDERS=[
            providers.Provider(
                name="test",
                label="sso.debian.org",
                options=kwargs,
            ),
        ]
    ):
        yield


class TestDistroTrackerSignon(TestCase):
    """Test DistroTrackerSignon."""

    identity: ClassVar[Identity]

    @classmethod
    def setUpTestData(cls) -> None:
        """Set up common test data."""
        super().setUpTestData()
        cls.identity = Identity.objects.create(
            issuer="test",
            subject="test@debian.org",
            claims={"name": "Test User", "email": "test@debian.org"},
        )

    def setUp(self) -> None:
        """Provide a mock unauthenticated request for tests."""
        super().setUp()
        self.factory = RequestFactory()
        self.request = self.factory.get("/")

    def test_create_user(self) -> None:
        """Create a user from an identity."""
        with provider_options():
            signon = DistroTrackerSignon(self.request)
            user = signon.create_user_from_identity(self.identity)
        assert user is not None

    def test_existing_user(self) -> None:
        """Match an existing user from an identity."""
        user = User.objects.create_user("test@debian.org")
        with provider_options():
            signon = DistroTrackerSignon(self.request)
            signon_user = signon.create_user_from_identity(self.identity)
        self.assertEqual(signon_user.id, user.id)

    def test_existing_user_email(self) -> None:
        """Match an existing user's secondary email address from an identity."""
        user = User.objects.create_user("other@example.org")
        UserEmail.objects.create(email="test@debian.org", user=user)
        with provider_options():
            signon = DistroTrackerSignon(self.request)
            signon_user = signon.create_user_from_identity(self.identity)
        self.assertEqual(signon_user.id, user.id)

    def test_create_user_no_user(self) -> None:
        """Test when Signon.create_user_from_identity fails."""
        with mock.patch(
            "distro_tracker.signon.distro_tracker.DistroTrackerSignon"
            ".create_user_from_identity",
            return_value=None,
        ):
            signon = DistroTrackerSignon(self.request)
            user = signon.create_user_from_identity(self.identity)
        self.assertIsNone(user)

    def test_create_user_no_provider(self) -> None:
        """Test when the identity has no provider."""
        with (
            provider_options(),
            mock.patch(
                "distro_tracker.signon.distro_tracker.DistroTrackerSignon"
                ".get_provider_for_identity",
                return_value=None,
            ),
        ):
            signon = DistroTrackerSignon(self.request)
            user = signon.create_user_from_identity(self.identity)
        assert user is not None
