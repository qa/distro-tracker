# Copyright 2023 The Debusine Developers
# See the COPYRIGHT file at the top-level directory of this distribution
#
# This file is part of Distro Tracker. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution and at https://deb.li/DTLicense. No part of Distro Tracker,
# including this file, may be copied, modified, propagated, or distributed
# except according to the terms contained in the LICENSE file.

"""Test of the signon authentication middleware for external providers."""

from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import ImproperlyConfigured, MiddlewareNotUsed
from django.http import HttpRequest, HttpResponse
from django.test import RequestFactory, TestCase, override_settings

from distro_tracker.signon import providers
from distro_tracker.signon.middleware import (
    RequestSignonProtocol,
    SignonMiddleware,
)
from distro_tracker.signon.signon import Signon

PROVIDERS_CONFIG = [
    providers.Provider(
        name="debsso",
        label="sso.debian.org",
    ),
    providers.Provider(name="salsa", label="Salsa"),
]


def mock_get_response(request: HttpRequest) -> HttpResponse:  # noqa: U100
    """Mock version of get_response to use for testing middlewares."""
    return HttpResponse()


class SignonSubclass(Signon):
    """Subclass of Signon to use in tests."""


class TestMiddleware(TestCase):
    """Test SignonMiddleware."""

    def setUp(self) -> None:
        """Provide a mock unauthenticated request for tests."""
        super().setUp()
        self.factory = RequestFactory()
        self.request = self.factory.get("/")

    @override_settings(SIGNON_PROVIDERS=PROVIDERS_CONFIG)
    def test_no_authentication_middleware(self) -> None:
        """Enforce that AuthenticationMiddleware is required."""
        mw = SignonMiddleware(get_response=mock_get_response)
        with self.assertRaises(ImproperlyConfigured):
            mw(self.request)

    @override_settings(SIGNON_PROVIDERS=[])
    def test_no_providers(self) -> None:
        """Skip middleware if there are no providers."""
        with self.assertRaises(MiddlewareNotUsed):
            SignonMiddleware(get_response=mock_get_response)

    @override_settings(SIGNON_PROVIDERS=PROVIDERS_CONFIG)
    def test_all_fine(self) -> None:
        """Test the case where all requirements are met."""
        self.request.user = AnonymousUser()
        assert not isinstance(self.request, RequestSignonProtocol)

        mw = SignonMiddleware(get_response=mock_get_response)
        mw(self.request)

        assert isinstance(self.request, RequestSignonProtocol)
        self.assertIsInstance(self.request.signon, Signon)

    @override_settings(
        SIGNON_PROVIDERS=PROVIDERS_CONFIG,
        SIGNON_CLASS="distro_tracker.does.not.Exist",
    )
    def test_signon_class_not_found(self) -> None:
        """Test setting SIGNON_CLASS to an invalid path."""
        with self.assertRaisesRegex(
            ImportError, r"No module named 'distro_tracker.does'"
        ):
            SignonMiddleware(get_response=mock_get_response)

    @override_settings(
        SIGNON_PROVIDERS=PROVIDERS_CONFIG,
        SIGNON_CLASS="distro_tracker.signon.models.Identity",
    )
    def test_signon_class_invalid(self) -> None:
        """Test setting SIGNON_CLASS to a non-Signon class."""
        with self.assertRaisesRegex(
            ImproperlyConfigured,
            "distro_tracker.signon.models.Identity is not a subclass "
            "of Signon",
        ):
            SignonMiddleware(get_response=mock_get_response)

    @override_settings(
        SIGNON_PROVIDERS=PROVIDERS_CONFIG,
        SIGNON_CLASS=f"{SignonSubclass.__module__}.SignonSubclass",
    )
    def test_signon_class(self) -> None:
        """Test setting SIGNON_CLASS to a valid subclass."""
        mw = SignonMiddleware(get_response=mock_get_response)
        self.assertIs(mw.signon_class, SignonSubclass)
